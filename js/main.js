// header dropdown

document.addEventListener("DOMContentLoaded", function() {
  document.querySelectorAll(".header__item-button").forEach(item => {
  item.addEventListener("click", function() {
    let btn = this;
    let dropdown = this.parentElement.querySelector(".header__dropdown");

    document.querySelectorAll(".header__item-button").forEach(el => {
      if (el != btn) {
        el.classList.remove("active-btn");
      }
    });

    document.querySelectorAll(".header__dropdown").forEach(el => {
      if (el != dropdown) {
        el.classList.remove("active-header__item");
      }
    })
    dropdown.classList.toggle("active-header__item");
    btn.classList.toggle("active-btn")
  })
})

document.addEventListener("click", function(e) {
  let target = e.target;
  if (!target.closest(".list")) {
    document.querySelectorAll(".header__dropdown").forEach(el => {
        el.classList.remove("active-header__item");
    })
     document.querySelectorAll(".header__item-button").forEach(el => {
        el.classList.remove("active-btn");
    });
  }
})
})
